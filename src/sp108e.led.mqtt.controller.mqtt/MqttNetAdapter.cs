﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using MQTTnet;
using MQTTnet.Client.Options;
using MQTTnet.Extensions.ManagedClient;
using sp108e.led.mqtt.controller.core.mqtt;

namespace sp108e.led.mqtt.controller.mqtt
{
    public class MqttNetAdapter : IMqttAdapter
    {
        private readonly IManagedMqttClient _mqtt;
        private readonly IManagedMqttClientOptions _options;
        private readonly ILogger<MqttNetAdapter> _logger;
        
        public MqttNetAdapter(ILogger<MqttNetAdapter> logger, IManagedMqttClient mqtt, IOptions<MqttConfig> mqttConfigOptions, IEnumerable<IMqttMessageHandler> handlers)
        {
            _mqtt = mqtt;
            _logger = logger;
            var mqttConfig = mqttConfigOptions.Value;

            var optionBuilder = new MqttClientOptionsBuilder()
                .WithClientId($"{mqttConfig.ClientId}-{Guid.NewGuid()}")
                .WithTcpServer(mqttConfig.Host, mqttConfig.Port);

            if (!string.IsNullOrWhiteSpace(mqttConfig.User))
            {
                optionBuilder = optionBuilder.WithCredentials(mqttConfig.User, mqttConfig.Password);
            }

            _options = new ManagedMqttClientOptionsBuilder()
                .WithAutoReconnectDelay(TimeSpan.FromSeconds(5))
                .WithClientOptions(optionBuilder.Build())
                .Build();

            _mqtt.UseConnectedHandler(e =>
            {
                _logger.LogDebug("Mqtt connected to {host}:{port}", mqttConfig.Host, mqttConfig.Port);
            });

            _mqtt.UseApplicationMessageReceivedHandler(async e =>
            {
                var payload = Encoding.UTF8.GetString(e.ApplicationMessage.Payload);
                var topic = e.ApplicationMessage.Topic;

                _logger.LogDebug("Mqtt message received topic='{topic}' payload='{payload}'", topic, payload);
                
                foreach (var handler in handlers.Where(x=>x.CanHandle(topic)))
                {
                    try
                    {
                        _logger.LogInformation("handle mqtt message payload={payload}, topic={topic}, handler={handler}",
                            payload,
                            topic,
                            handler.GetType().Name);

                        await handler.HandleAsync(new MqttMessage
                        {
                            Payload = payload,
                            Topic = topic
                        }, this);
                    }
                    catch (Exception ex)
                    {
                        _logger.LogError(ex,
                            "Error handle mqtt message payload={payload}, topic={topic}, handler={handler}",
                            payload,
                            topic,
                            handler.GetType().Name);
                    }
                }
            });
        }

        public async Task ConnectAsync()
        {
            _logger.LogDebug("Mqtt start");
            if (_mqtt.IsConnected)
            {
                _logger.LogDebug("Already connected");
                return;
            }

            await _mqtt.StartAsync(_options);
        }

        public async Task DisconnectAsync()
        {
            _logger.LogDebug("Mqtt stop");
            if (!_mqtt.IsConnected)
            {
                _logger.LogDebug("Already disconnected");
                return;
            }

            await _mqtt.StopAsync();
        }

        public Task SubscribeAsync(string topic)
        {
            return _mqtt.SubscribeAsync(topic);
        }

        public Task PublishAsync(string topic, string payload, bool retain = false)
        {
            return _mqtt.PublishAsync(new MqttApplicationMessage
            {
                Payload = Encoding.UTF8.GetBytes(payload),
                Retain = retain,
                Topic = topic
            });
        }
    }
}
