﻿using System;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using MQTTnet;
using MQTTnet.Extensions.ManagedClient;
using sp108e.led.mqtt.controller.core.mqtt;

namespace sp108e.led.mqtt.controller.mqtt.extensions
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddMqttNet(this IServiceCollection services, IConfiguration config, params Type[] handlerTypesMarkers)
        {
            services.Configure<MqttConfig>(config.GetSection(nameof(MqttConfig)));
            services.AddSingleton(new MqttFactory().CreateManagedMqttClient());
            services.AddSingleton<IMqttAdapter, MqttNetAdapter>();

            services.Scan(scan => scan
                .FromAssembliesOf(handlerTypesMarkers)
                .AddClasses(classes => classes.AssignableTo<IMqttMessageHandler>())
                .AsImplementedInterfaces()
                .WithTransientLifetime());

            return services;
        }
    }
}
