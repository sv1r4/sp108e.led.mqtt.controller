﻿using System.Threading.Tasks;

namespace sp108e.led.mqtt.controller.core.sp108
{
    public interface ISp108Client
    {
        Task<string> GetNameAsync();
        Task<Sp108Settings> GetSettingsAsync();
        Task SetSpeedAsync(byte speed);
        Task SetBrightnessAsync(byte value);
        Task SetRgbModeAsync(byte modeNum);
        Task PowerOffAsync();
        Task PowerOnAsync();
    }
}
