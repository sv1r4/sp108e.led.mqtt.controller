﻿namespace sp108e.led.mqtt.controller.core.sp108
{
    public class Sp108Settings
    {
        public bool IsOn { get; set; }
        public byte Animation { get; set; }
        public byte Speed { get; set; }
        public byte Brightness { get; set; }
        public byte ColorOrder { get; set; }
        public int LedsPerSegment { get; set; }
        public int SegmentsCount { get; set; }
        public (byte R, byte G, byte B) MonoColor { get; set; }
        public byte IcType { get; set; }
        public byte PatternsCount { get; set; }
        public byte WhiteBrightness { get; set; }
    }
}
