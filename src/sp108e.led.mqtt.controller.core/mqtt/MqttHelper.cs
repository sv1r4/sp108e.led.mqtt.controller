﻿namespace sp108e.led.mqtt.controller.core.mqtt
{
    public static class MqttHelper
    {
        public static string CombineTopic(string topic, string subTopic)
        {
            return string.Join('/', topic.TrimEnd('/'), subTopic.TrimStart('/'));
        }
    }
}
