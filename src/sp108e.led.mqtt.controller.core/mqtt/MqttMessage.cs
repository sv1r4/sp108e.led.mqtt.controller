﻿namespace sp108e.led.mqtt.controller.core.mqtt
{

    public class MqttMessage
    {
        public string Topic { get; set; }
        public string Payload { get; set; }
    }
}