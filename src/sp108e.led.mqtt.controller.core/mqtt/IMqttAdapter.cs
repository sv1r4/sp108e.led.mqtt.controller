﻿using System.Threading.Tasks;

namespace sp108e.led.mqtt.controller.core.mqtt
{
    public interface IMqttAdapter
    {
        Task ConnectAsync();
        Task DisconnectAsync();
        Task SubscribeAsync(string topic);
        Task PublishAsync(string topic, string payload, bool retain = false);
    }
}