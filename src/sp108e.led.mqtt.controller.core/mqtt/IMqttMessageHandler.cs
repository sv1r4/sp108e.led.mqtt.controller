﻿using System.Threading.Tasks;

namespace sp108e.led.mqtt.controller.core.mqtt
{
    public interface IMqttMessageHandler
    {
        bool CanHandle(string topic);

        Task HandleAsync(MqttMessage msg, IMqttAdapter mqtt);
    }
}
