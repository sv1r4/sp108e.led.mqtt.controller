﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using sp108e.led.mqtt.controller.core;
using sp108e.led.mqtt.controller.core.sp108;

namespace sp108e.led.mqtt.controller.tcp.extensions
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddSp108TcpClient(this IServiceCollection services, IConfiguration config)
        {
            services.Configure<TcpConfig>(config.GetSection(nameof(TcpConfig)));
            
            services.AddTransient<ISp108Client, Sp108TcpClient>();

            return services;
        }
    }
}
