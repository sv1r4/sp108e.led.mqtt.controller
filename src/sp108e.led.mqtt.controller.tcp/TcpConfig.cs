﻿using System;

namespace sp108e.led.mqtt.controller.tcp
{
    public class TcpConfig
    {
        public string Ip { get; set; }
        public int Port { get; set; }
    }
}
