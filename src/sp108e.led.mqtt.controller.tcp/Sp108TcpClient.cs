﻿using System;
using System.IO;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using sp108e.led.mqtt.controller.core.sp108;
using sp108e.led.mqtt.controller.tcp.constants;

namespace sp108e.led.mqtt.controller.tcp
{
    public class Sp108TcpClient:ISp108Client, IDisposable
    {
        private readonly TcpConfig _options;

        private TcpClient _tcp;

        private const int ResponseDeviceNameLength = 18;
        private const int ResponseSettingsLength = 17;

        private readonly TimeSpan _readTimeout = TimeSpan.FromSeconds(2);

        public Sp108TcpClient(IOptions<TcpConfig> options)
        {
            _options = options.Value;
        }

        private Stream GetTcpStream()
        {
            if (_tcp?.Connected == true)
            {
                return _tcp.GetStream();
            }

            _tcp?.Close();
            _tcp =  new TcpClient(_options.Ip, _options.Port);

            return _tcp.GetStream();
        }

        public async Task<string> GetNameAsync()
        {
            await using var s = GetTcpStream();

            await SendCommandAsync(s,  Instructions.GetName);
            
            using var cts = new CancellationTokenSource(_readTimeout);

            var responseBuffer = new byte[ResponseDeviceNameLength];

            await s.ReadAsync(responseBuffer.AsMemory(0, ResponseDeviceNameLength), cts.Token);

            return Encoding.UTF8.GetString(responseBuffer, 0,ResponseDeviceNameLength).Replace("\0","");
        }

        public async Task<Sp108Settings> GetSettingsAsync()
        {
            await using var s = GetTcpStream();

            await SendCommandAsync(s,Instructions.GetSettings);
            
            using var cts = new CancellationTokenSource(_readTimeout);

            var responseBuffer =new byte[ResponseSettingsLength];

            await s.ReadAsync(responseBuffer.AsMemory(0, ResponseSettingsLength), cts.Token);

            return new Sp108Settings
            {
                IsOn = responseBuffer[1] != 0,
                Animation = responseBuffer[2],
                Speed = responseBuffer[3],
                Brightness = responseBuffer[4],
                ColorOrder = responseBuffer[5],
                LedsPerSegment = BitConverter.ToInt16(new []{responseBuffer[7],responseBuffer[6]}),
                SegmentsCount = BitConverter.ToInt16(new []{responseBuffer[9],responseBuffer[8]}),
                MonoColor = (responseBuffer[10], responseBuffer[11], responseBuffer[12]),
                IcType = responseBuffer[13],
                PatternsCount = responseBuffer[14],
                WhiteBrightness = responseBuffer[15]
            };
        }

        private async Task SendPowerOnOffAsync()
        {
            await SendCommandAsync(Instructions.SetOnOff);
            //delay required before next command send
            await Task.Delay(500);
        }

        public async Task PowerOffAsync()
        {
            var s = await GetSettingsAsync();
            if (s.IsOn) { await SendPowerOnOffAsync(); }
        }

        public async Task PowerOnAsync()
        {
            var s = await GetSettingsAsync();
            if (!s.IsOn) { await SendPowerOnOffAsync(); }
        }

        public Task SetSpeedAsync(byte speed)
        {
            return SendCommandAsync(Instructions.SetSpeed, speed);
        }

        public async Task SetBrightnessAsync(byte value)
        {
            await using var  s = GetTcpStream();

            await SendCommandAsync(Instructions.SetBrightness, value);
        }

        public Task SetRgbModeAsync(byte modeNum)
        {
            return SendCommandAsync(Instructions.SetRgbMode, modeNum);
        }

        private async Task SendCommandAsync(byte instruction, 
            byte arg1 = 0, 
            byte arg2 = 0,
            byte arg3 = 0)
        {
            await using var s = GetTcpStream();

            await SendCommandAsync(s, instruction, arg1, arg2, arg3);
        }

        private static async Task SendCommandAsync(Stream s, byte instruction, 
            byte arg1 = 0, 
            byte arg2 = 0,
            byte arg3 = 0)
        {
            await s.WriteAsync(new byte[]{0x38, arg1, arg2, arg3, instruction, 0x83}.AsMemory(0, 6));
        }


        public void Dispose()
        {
            _tcp?.Close();
        }
    }
}
