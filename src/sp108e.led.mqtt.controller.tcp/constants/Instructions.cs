﻿namespace sp108e.led.mqtt.controller.tcp.constants
{
    public static class Instructions
    {
        public static readonly byte GetName = 0x77;
        public static readonly byte GetSettings = 0x10;
        public static readonly byte SetSpeed = 0x03;
        public static readonly byte SetBrightness = 0x2a;
        public static readonly byte SetOnOff = 0xaa;
        public static readonly byte SetRgbMode = 0x2c;
    }
}
