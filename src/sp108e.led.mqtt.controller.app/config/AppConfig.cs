﻿using System;

namespace sp108e.led.mqtt.controller.app.config
{
    public class AppConfig
    {
        public string SubscribeTopic { get; set; }
        public string PublishTopic { get; set; }
        public TimeSpan PubStateInterval { get; set; } = TimeSpan.FromSeconds(10);
    }
}
