﻿using System;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using sp108e.led.mqtt.controller.app.config;
using sp108e.led.mqtt.controller.core.mqtt;
using sp108e.led.mqtt.controller.core.sp108;

namespace sp108e.led.mqtt.controller.app
{
    public class App : IHostedService
    {
        private readonly IMqttAdapter _mqtt;
        private readonly AppConfig _config;
        private readonly ISp108Client _sp108;
        private readonly string _pubSettingsTopic;
        private readonly string _pubStateTopic;
        private readonly ILogger<App> _logger;

        private static readonly JsonSerializerOptions JsonSettings = new JsonSerializerOptions
        {
            IgnoreNullValues = false,
            WriteIndented = true
        };

        private CancellationTokenSource  _daemonCancellation;

        public App(IMqttAdapter mqtt, IOptions<AppConfig> config, ISp108Client sp108, ILogger<App> logger)
        {
            _mqtt = mqtt;
            _sp108 = sp108;
            _logger = logger;
            _config = config.Value;

            _pubSettingsTopic = MqttHelper.CombineTopic(config.Value.PublishTopic, "settings");
            _pubStateTopic = MqttHelper.CombineTopic(config.Value.PublishTopic, "state");
        }

        public async Task StartAsync(CancellationToken cancellationToken)
        {
            await _mqtt.ConnectAsync();
            await _mqtt.SubscribeAsync(_config.SubscribeTopic);
        
            _daemonCancellation = new CancellationTokenSource();

            StartPubStateDaemon();
        }

        private void StartPubStateDaemon()
        {
            Task.Run(async () =>
            {
                try
                {
                    while (!_daemonCancellation.IsCancellationRequested)
                    {
                        await Task.Delay(_config.PubStateInterval, _daemonCancellation.Token);

                        Sp108Settings settingsData;
                        try
                        {
                            settingsData = await _sp108.GetSettingsAsync();
                        }
                        catch (Exception ex)
                        {
                            _logger.LogError(ex, "error get SP108E settings");
                            continue;
                        }

                        try
                        {
                            await _mqtt.PublishAsync(_pubSettingsTopic,
                                JsonSerializer.Serialize(settingsData, JsonSettings));

                            await _mqtt.PublishAsync(_pubStateTopic, settingsData.IsOn ? "1" : "0");
                        }
                        catch (Exception ex)
                        {
                            _logger.LogError(ex, "error publish state");
                        }
                    }
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, "Error executing background pub state daemon");
                }
            }, _daemonCancellation.Token);
        }
        public async Task StopAsync(CancellationToken cancellationToken)
        {
            _daemonCancellation.Cancel();
            await _mqtt.DisconnectAsync();
        }
    }
}
