﻿using Microsoft.Extensions.Options;
using sp108e.led.mqtt.controller.app.config;
using sp108e.led.mqtt.controller.core.mqtt;
using sp108e.led.mqtt.controller.core.sp108;

namespace sp108e.led.mqtt.controller.app.handlers
{
    public abstract class BaseHandler
    {
        protected readonly ISp108Client Sp108;
        protected readonly AppConfig Config;

        protected BaseHandler(ISp108Client sp108, IOptions<AppConfig> config)
        {
            Sp108 = sp108;
            Config = config.Value;
        }

        protected string GetTopic(string topTopic)
        {
            return MqttHelper.CombineTopic(Config.PublishTopic, topTopic);
        }
    }
}
