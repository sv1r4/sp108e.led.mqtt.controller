﻿using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using sp108e.led.mqtt.controller.app.config;
using sp108e.led.mqtt.controller.core.mqtt;
using sp108e.led.mqtt.controller.core.sp108;

namespace sp108e.led.mqtt.controller.app.handlers
{
    public class SpeedHandler : BaseHandler,  IMqttMessageHandler
    {
        public SpeedHandler(ISp108Client sp108, IOptions<AppConfig> config)
            : base(sp108, config)
        {
        }

        public bool CanHandle(string topic)
        {
            return topic.EndsWith("/speed");
        }

        public async Task HandleAsync(MqttMessage msg, IMqttAdapter mqtt)
        {
            if (!byte.TryParse(msg.Payload, out var value))
            {
                throw new InvalidOperationException($"Cant convert payload ='{msg.Payload}' to byte");
            }

            var pubTopic = GetTopic("speed");
            
            await Sp108.SetSpeedAsync(value);
            await mqtt.PublishAsync(pubTopic, value.ToString());
        }
    }
}
