﻿using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using sp108e.led.mqtt.controller.app.config;
using sp108e.led.mqtt.controller.core.mqtt;
using sp108e.led.mqtt.controller.core.sp108;

namespace sp108e.led.mqtt.controller.app.handlers
{
    public class OnOffHandler : BaseHandler,  IMqttMessageHandler
    {
        public OnOffHandler(ISp108Client sp108, IOptions<AppConfig> config)
            : base(sp108, config)
        {
        }

        public bool CanHandle(string topic)
        {
            return topic.EndsWith("/state");
        }

        public async Task HandleAsync(MqttMessage msg, IMqttAdapter mqtt)
        {
            var pubTopic = GetTopic("state");
            if (msg.Payload[0] == '0' || msg.Payload[0] == 0)
            {
                await Sp108.PowerOffAsync();
                await mqtt.PublishAsync(pubTopic, "0");
            }
            else
            {
                await Sp108.PowerOnAsync();
                await mqtt.PublishAsync(pubTopic, "1");
            }
        }
    }
}
