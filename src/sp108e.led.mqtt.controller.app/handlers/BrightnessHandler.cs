﻿using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using sp108e.led.mqtt.controller.app.config;
using sp108e.led.mqtt.controller.core.mqtt;
using sp108e.led.mqtt.controller.core.sp108;

namespace sp108e.led.mqtt.controller.app.handlers
{
    public class BrightnessHandler : BaseHandler,  IMqttMessageHandler
    {
        public BrightnessHandler(ISp108Client sp108, IOptions<AppConfig> config)
            : base(sp108, config)
        {
        }

        public bool CanHandle(string topic)
        {
            return topic.EndsWith("/brightness");
        }

        public async Task HandleAsync(MqttMessage msg, IMqttAdapter mqtt)
        {
            if (!byte.TryParse(msg.Payload, out var value))
            {
                throw new InvalidOperationException($"Cant convert payload ='{msg.Payload}' to byte");
            }

            var pubTopic = GetTopic("brightness");
            
            await Sp108.SetBrightnessAsync(value);
            await mqtt.PublishAsync(pubTopic, value.ToString());
        }
    }
}
