﻿using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using sp108e.led.mqtt.controller.app.config;
using sp108e.led.mqtt.controller.app.handlers;
using sp108e.led.mqtt.controller.mqtt.extensions;
using sp108e.led.mqtt.controller.tcp.extensions;

namespace sp108e.led.mqtt.controller.app
{
    public class Program
    {
        public static async Task Main(string[] args)
        {
            await CreateHostBuilder(args).Build().RunAsync();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureServices((ctx, ss) =>
                {
                    ss.Configure<AppConfig>(ctx.Configuration.GetSection(nameof(AppConfig)));

                    ss.AddHostedService<App>();

                    ss.AddSp108TcpClient(ctx.Configuration);

                    ss.AddMqttNet(ctx.Configuration, typeof(BaseHandler));
                });
    }
}
