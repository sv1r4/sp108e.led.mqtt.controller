using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using sp108e.led.mqtt.controller.core.sp108;
using sp108e.led.mqtt.controller.tcp.extensions;
using Xunit;
using Xunit.Abstractions;

namespace sp108e.led.mqtt.controller.tcp.intergationtests
{
    public class Sp108TcpClientTest
    {
        private readonly ITestOutputHelper _testOutputHelper;
        private readonly IServiceProvider _serviceProvider;

        public Sp108TcpClientTest(ITestOutputHelper testOutputHelper)
        {
            _testOutputHelper = testOutputHelper;

            var config = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json")
                .AddEnvironmentVariables()
                .Build()
                ;

            var services = new ServiceCollection()
                .AddSp108TcpClient(config)
                ;

            _serviceProvider = services.BuildServiceProvider();
        }

        [Fact]
        public void Sp108TcpClient_Resolved()
        {
            var client = _serviceProvider.GetRequiredService<ISp108Client>();

            Assert.NotNull(client as Sp108TcpClient);
        }

        [Fact]
        public async Task GetNameAsync_NameReturned()
        {
            var client = _serviceProvider.GetRequiredService<ISp108Client>();
            var deviceName = await client.GetNameAsync();

            Assert.NotNull(deviceName);

            _testOutputHelper.WriteLine("Device Name");
            _testOutputHelper.WriteLine(deviceName);
        }
        
        [Fact]
        public async Task GetSettingsAsync_SettingsReturned()
        {
            var client = (Sp108TcpClient)_serviceProvider.GetRequiredService<ISp108Client>();
            var deviceSettings = await client.GetSettingsAsync();

            Assert.NotNull(deviceSettings);

            _testOutputHelper.WriteLine("Device Settings");
            _testOutputHelper.WriteLine(JsonConvert.SerializeObject(deviceSettings, Formatting.Indented));
        }

        [Fact]
        public async Task PowerOffAsync_SettingsReturned()
        {
            var client =  (Sp108TcpClient)_serviceProvider.GetRequiredService<ISp108Client>();
            await client.PowerOffAsync();

            var s = await client.GetSettingsAsync();
            Assert.False(s.IsOn);
        }

        [Fact]
        public async Task PowerOnAsync_SettingsReturned()
        {
            var client =  (Sp108TcpClient)_serviceProvider.GetRequiredService<ISp108Client>();
            await client.PowerOnAsync();

            var s = await client.GetSettingsAsync();
            Assert.True(s.IsOn);
        }

        [Fact]
        public async Task SetSpeedAsync_Success()
        {
            var client =  (Sp108TcpClient)_serviceProvider.GetRequiredService<ISp108Client>();
            await client.SetSpeedAsync(10);

            await Task.Delay(3000);

            await client.SetSpeedAsync(255);

            Assert.True(true);
        }

        [Fact]
        public async Task SetBrightnessAsync_Success()
        {
            var client = (Sp108TcpClient)_serviceProvider.GetRequiredService<ISp108Client>();
            await client.SetBrightnessAsync(10);

            await Task.Delay(3000);

            await client.SetBrightnessAsync(150);

            Assert.True(true);
        }

        [Fact]
        public async Task SetRgbModeAsync_Success()
        {
            var client =  (Sp108TcpClient)_serviceProvider.GetRequiredService<ISp108Client>();
            await client.SetRgbModeAsync(6);

            await Task.Delay(3000);

            await client.SetRgbModeAsync(0);

            Assert.True(true);
        }
    }
}
